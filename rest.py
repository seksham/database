import requests
import json
# #import python-openstackclient

# from openstack import connection
# auth_args = {
#     'auth_url': 'http://192.168.30.148:5000/v2.0',
#     'project_name': 'admin',
#     'username': 'admin',
#     'password': '76a5fe23022b4e73',
# }
# conn = connection.Connection(**auth_args)
# token=conn.authorize()
# print(token)
#KEYSTONE
file=open('endpoints.json','w+')
body='{"auth": { "tenantName": "admin","passwordCredentials": {"username": "admin",  "password": "a"   }}}'
url='http://192.168.30.148:5000/v2.0/tokens'
headers={'Content-Type': 'application/json'}
r=requests.post(url=url,headers=headers, data=body)
print(r.status_code)
parsed = json.loads(r.text)
file.write(json.dumps(parsed, indent=4, sort_keys=True))

#TOKEN
dic=json.loads(r.text)
token=dic['access']['token']['id']
print(token)

#GLANCE
header2={"X-Auth-Token": token}
url="http://192.168.30.148:9292/v2/images"
image=requests.get(url=url,headers=header2)
print(image.status_code)
print(image.text)
dic=json.loads(image.text)
file3=open('images.json','w+')
parsed = json.loads(image.text)
file3.write(json.dumps(parsed, indent=4, sort_keys=True))
file3.close()
print(dic['images'][0]['name'])
# for i in range(len(dic['images'])):
#     print(dic['images'][i]['name'])
choices = [(i,dic['images'][i]['name'] ) for i in range(len(dic['images']))]
print(choices)

#NEUTRON
url="http://192.168.30.148:9696/v2.0/networks"
net=requests.get(url=url,headers=header2)
print(type(net))
print(type(net.json()))
print(net.status_code)
print(net.text)
file2=open('networks.json','w+')
parsed = json.loads(net.text)
file2.write(json.dumps(parsed, indent=4, sort_keys=True))
file2.close()
#NOVA
url="http://192.168.30.148:8774/v2.1/e11e48a5a45d4bb08bdf864ad8ab9b68/servers?all_tenants=1"
instance=requests.get(url=url,headers=header2)
print(instance.status_code)
print(instance.text)
file5=open('instances.json','w+')
parsed = json.loads(instance.text)
file5.write(json.dumps(parsed, indent=4, sort_keys=True))
file5.close()

# body='{"server": {   "name": "auto-allocate-network",  "imageRef": "6b471aea-b261-4f26-9591-e1acc2e1c415",   "flavorRef": "http://openstack.example.com/flavors/1",    "networks": [{"uuid": "2bb7f2bc-d80b-4579-9b0d-f205c5f6d11c"}]  }}'
# url="http://192.168.30.148:8774/v2.1/e11e48a5a45d4bb08bdf864ad8ab9b68/servers"
#
# svr=requests.post(url=url,data=body,headers=header2)
# print(svr.status_code)
# print(svr.text)

#delete
# delt=requests.delete(url=url,headers=header2)

#CINDER
url="http://192.168.30.148:8776/v3/e11e48a5a45d4bb08bdf864ad8ab9b68/volumes/detail"
volume=requests.get(url=url,headers=header2)
print(volume.status_code)
print(volume.text)
file4=open('volumes.json','w+')
parsed = json.loads(volume.text)
file4.write(json.dumps(parsed, indent=4, sort_keys=True))
file4.close()