from webbrowser import get

import cx_Oracle
from flask import Flask, render_template,request
from wtforms import Form, StringField, PasswordField, validators, TextAreaField,Label
conn = cx_Oracle.connect("hr/hr@192.168.30.148:1521/xe")
cursor = conn.cursor()


# a=''
# for fname, lname in cursor:
#     a=a+fname+" "+lname+', '
# print(a)
app=Flask(__name__)


#print(file.readline())
@app.route("/pdfs")
def table():
    file = open("urls.text", "r+")
    return render_template('pdfs.html',data=file.readlines())

@app.route("/tables")
def pdf():
    cursor.execute('SELECT first_name, last_name FROM employees')
    return render_template('names.html',data1=cursor)

@app.route("/")
def home():
    return render_template('home.html')


@app.route("/form", methods=['GET','POST'])
def reg():
    form=regForm(request.form)

    if request.method=='POST' and form.validate():
        f=form.fname.data
        l=form.lname.data
        cursor.execute('SELECT last_name FROM employees WHERE first_name=:name',{'name':f})
        a=''
        # for name in cursor:
        #     a=a+str(name)
        # print(a)
        a=cursor.fetchall()[0][0]
        form.lname.data=a
        return render_template('form.html',form=form)
    return render_template('form.html', form=form)

class regForm(Form):
    fname = StringField('First Name', validators=[validators.input_required()])
    lname  = StringField('Last Name', validators=[validators.optional()])


if __name__=='__main__':
    app.run(host="0.0.0.0",debug=True)