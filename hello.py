import requests
import json
url = 'http://httpbin.org/ip'
response = requests.get(url)

print("GET DATA")
print(response.status_code)
print(response)
print(type(response))
sid=response.json()
print(type(sid))
# //to extract the detail from response
print(sid)
dic=json.loads(response.text)
for key,value in dic.items():
    print('key is %s, value is %s'%(key,value))
print()
data={
    "comments": "hii",
    "custemail": "saksham.bhayana@gslab.com",
    "custname": "saksham",
    "custtel": "123456789",
    "delivery": "pune",
    "size": "small",
    "topping": "bacon"
  }

print("POST DATA")
postdata=requests.post("http://httpbin.org/post",data=data)
print(postdata.status_code)
print(postdata.text)

d=json.loads(postdata.text)
print("PUT DATA")
r = requests.put('http://httpbin.org/put', data = {'custname':'xyz'})
print(r.status_code)
print(r.text)

print("DELETE DATA")
r = requests.delete('http://httpbin.org/delete')
print(r.status_code)
print(r.text)

print("HEADER DATA")
r = requests.head('http://httpbin.org/get')
print(r.status_code)
print(r.text)

print("OPTIONS DATA")
r = requests.options('http://httpbin.org/get')
print(r.status_code)
print(r.text)





